#ifndef GRESULT_H
#define GRESULT_H

#include<sstream>
#include<iostream>
#include<string.h>

using namespace std;

class GResult
{
	private:
		int round;
		string status;
		int bankPoints;
		int bankCardCount;
		int playerPoints;
		int playerCardCount;
		string description;
	public:
		GResult();
		GResult(int, string, int, int, string, int, int);
		~GResult();
		void setRounds(int round);
		int getRounds();
		void setStatus(string status);
		string getStatus();
		void setBankPoints(int bankPoints);
		int getBankPoints();
		void setPLayerPoints(int playerPoints);
		int getPlayerPonits();
		void setDescription(string);
		string getDescription();
		void setBankCardCount(int);
		int getBankCardCount();
		void setPlayerCardCount(int);
		int getPlayerCardCount();
		
		void setData(int, string, int, int, string, int, int);
		ostringstream displayGResult();
		
};

#endif