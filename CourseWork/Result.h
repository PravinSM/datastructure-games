#ifndef RESULT_H
#define RESULT_H

#include<sstream>
#include<iostream>
#include<string.h>

using namespace std;

class Result
{
	private:
		int rounds;
		string status;
		int bankPoints;
		int playerPoints;
		string description;
	public:
		Result();
		Result(int, string, int, int, string);
		~Result();
		void setRounds(int round);
		int getRounds();
		void setStatus(string status);
		string getStatus();
		void setBankPoints(int bankPoints);
		int getBankPoints();
		void setPLayerPoints(int playerPoints);
		int getPlayerPonits();
		void setDescription(string);
		string getDescription();
		
		string displayResult();
		
};

#endif