#include"Cards.h"

Cards::Cards()
{
	
}

Cards::Cards(string face,string suite)
{
	this->face=face;
	this->suite=suite;
	//[Validation] - Checking if the correct values are getting passed to constructors
	//cout<<"\nFace :\t"<<this->face<<"\nSuite :\t"<<this->suite;
}

Cards::~Cards()
{
	
}

void Cards::setFace(string face)
{
	this->face=face;
}

string Cards::getFace()
{
	return(face);
}

void Cards::setSuite(string suite)
{
	this->suite=suite;
}

string Cards::getSuite()
{
	return(suite);
}

int Cards::convertCardToPoints(Cards* card)
{
	int suitePoints=convertCardSuiteToPoints(card);
	int facePoints=convertCardFaceToPoints(card);
	return(suitePoints+facePoints);
}

int Cards::convertCardSuiteToPoints(Cards* card)
{
	int suitePoints;
	if(card->getSuite()=="SPADE")
		suitePoints=400;
	if(card->getSuite()=="CLUBS")
		suitePoints=300;
	if(card->getSuite()=="HEARTS")
		suitePoints=200;
	if(card->getSuite()=="DIMOND")
		suitePoints=100;
	return(suitePoints);
}

int Cards::convertCardFaceToPoints(Cards* card)
{
	int facePoints;
	if(card->getFace()=="ACE")
		facePoints=13;
	if(card->getFace()=="KING")
		facePoints=12;
	if(card->getFace()=="QUEEN")
		facePoints=11;
	if(card->getFace()=="JACK")
		facePoints=10;
	if(card->getFace()=="TEN")
		facePoints=9;
	if(card->getFace()=="NINE")
		facePoints=8;
	if(card->getFace()=="EIGHT")
		facePoints=7;
	if(card->getFace()=="SEVEN")
		facePoints=6;
	if(card->getFace()=="SIX")
		facePoints=5;
	if(card->getFace()=="FIVE")
		facePoints=4;
	if(card->getFace()=="FOUR")
		facePoints=3;
	if(card->getFace()=="THREE")
		facePoints=2;
	if(card->getFace()=="TWO")
		facePoints=1;
	return(facePoints);
}

int Cards::convertBCardFaceToPoints(Cards* card)
{
	int facePoints;
	
	if(card->getFace()=="KING")
		facePoints=10;
	if(card->getFace()=="QUEEN")
		facePoints=10;
	if(card->getFace()=="JACK")
		facePoints=10;
	if(card->getFace()=="TEN")
		facePoints=10;
	if(card->getFace()=="NINE")
		facePoints=9;
	if(card->getFace()=="EIGHT")
		facePoints=8;
	if(card->getFace()=="SEVEN")
		facePoints=7;
	if(card->getFace()=="SIX")
		facePoints=6;
	if(card->getFace()=="FIVE")
		facePoints=5;
	if(card->getFace()=="FOUR")
		facePoints=4;
	if(card->getFace()=="THREE")
		facePoints=3;
	if(card->getFace()=="TWO")
		facePoints=2;
	if(card->getFace()=="ACE")
		facePoints=1;
	return(facePoints);
}

void Cards::showCard()
{
	cout<<face<<" OF "<<suite;
}

