#include"CardGuessing.h"

CardGuessing::~CardGuessing()
{
	
}

void CardGuessing::start()
{
	char option;
	do
	{
		system("CLS");
		CardOperation* cOperation;
	
		//Creating a pack of fair deck of Cards.
		Node* node=cOperation->createDeck();
	
		//Mixing the deck of Cards
		cOperation->mixDeck(node);
		//[Validation] - Displaying deck after mixing the cards
		//cOperation->displayDeck(node);
		
		//Picking a random card
		cpCard=cOperation->pickRandomCard(node);
		
		//[Validation] - Displaying the random Card picked by Computer
		//cout<<"\nComputer Picked Card is :\t";
		//cpCard->getData()->showCard();
		
		int validGuessCount=0;
		int compare;
		do
		{
			//Calling the Guess Card Method to make my Selection/Guess from the list of available options
			guessCard();
			
			compare=compareCards(cOperation,validGuessCount);
			
			
		}while(compare!=0);
		
		system("CLS");
		displayAllValidGuess(validGuessCount);
		
		/*[Validation] - Checking if withdraw function is working properly or not
		Node* cWithdrawn=new Node();
		node=cOperation->withdrawCard(node,cWithdrawn);
		cout<<"\nPicked Card is :\t";
		cWithdrawn->getData()->showCard();
		*/
		
		//checking if user wants to continue with another game
		cout<<"\nDo you wish to play another game ? (Y/N) \t";
		//cin>>option;
		option=getch();
	}while(option=='Y'||option=='y');
}

void CardGuessing::guessCard()
{
	cout<<flush;
	string suite=guessSuite();
	cout<<flush;
	string face=guessFace();
	guess=new Node(new Cards(face,suite));
}

string CardGuessing::guessSuite()
{
	int suite;
	string input;
	do
	{
		system("CLS");
		cout<<"\n-------------------------------------------------------------\n";
		cout<<"\n\n                       Rules                               \n\n";
		cout<<"\n-------------------------------------------------------------\n";
		cout<<"\nYou can Make any possible guess from the Four Suites\n1. SPADE\n2. CLUBS\n3. HEARTS\n4. DIMOND";
		cout<<"\n _____________________________________________________";
		cout<<"\n|                         Note                        |";
		cout<<"\n|_____________________________________________________|";
		cout<<"\n|This above order is sorted in Descending             |";
		cout<<"\n|   1. SPADE is bigger compared to all other 3 group  |";
		cout<<"\n|   2. CLUBS is greater than HEARTS and DIMOND        |";
		cout<<"\n|   3. HEARTS is greater than DIMOND                  |";
		cout<<"\n|   4. DIMOND is the smallest of them                 |";
		cout<<"\n|_____________________________________________________|\n";
		cout<<"\nYour Choice is :\t";
		cout<<flush;
		suite=validateStringInput();
	}while(suite<1||suite>5);
	switch(suite)
	{
		case 1:
			return("SPADE");
		case 2:
			return("CLUBS");
		case 3:
			return("HEARTS");
		case 4:
			return("DIMOND");
	}
}

string CardGuessing::guessFace()
{
	int face;
	string input;
	do
	{
		system("CLS");
		cout<<"\n-------------------------------------------------------------\n";
		cout<<"\n\n                       Rules                               \n\n";
		cout<<"\n-------------------------------------------------------------\n";
		cout<<"\nYou can Make any possible guess from the 13 Face\n1. ACE\n2. KING\n3. QUEEN\n4. JACK\n5. TEN\n6. NINE\n7. EIGHT\n8. SEVEN\n9. SIX\n10. FIVE\n11. FOUR\n12 THREE\n13 TWO";
		cout<<"\n __________________________________________________";
		cout<<"\n|                      Note                        |";
		cout<<"\n|__________________________________________________|";
		cout<<"\n|                                                  |";
		cout<<"\n|This above order is sorted in Descending          |";
		cout<<"\n|   1. ACE is bigger compared to all other FACES   |";
		cout<<"\n|   2. KING is bigger compared to QUEEN and JACK   |";
		cout<<"\n|   3. QUEEN is bigger compared to JACK            |";
		cout<<"\n|   4. JACK is bigger than rest of the numbers     |";
		cout<<"\n|__________________________________________________|\n";
		cout<<"\nYour Choice is :\t";
		cout<<flush;
		face=validateStringInput();
	}while(face<1||face>14);
	switch(face)
	{
		case 1:
			return("ACE");
		case 2:
			return("KING");
		case 3:
			return("QUEEN");
		case 4:
			return("JACK");
		case 5:
			return("TEN");
		case 6:
			return("NINE");
		case 7:
			return("EIGHT");
		case 8:
			return("SEVEN");
		case 9:
			return("SIX");
		case 10:
			return("FIVE");
		case 11:
			return("FOUR");
		case 12:
			return("THREE");
		case 13:
			return("TWO");
	}
}

int CardGuessing::compareCards(CardOperation* cOperation,int& validGuessCount)
{
	
	int compareCardFace=cOperation->compareFace(cpCard->getData(),guess->getData());
	int compareCardSuite=cOperation->compareSuite(cpCard->getData(),guess->getData());
	int compare=1;
	
	if(compareCardFace==0&&compareCardSuite==0)
	{
		cout<<"\nYou Nailed It...\nHurray...Congrats...You Guessed it Right!!!";
		compare=0;
	}
	else
	{
		if(compareCardFace==-1&&compareCardSuite==-1)
		{
			cout<<"Both Face and the Suite of the Card that you picked(";
			guess->getData()->showCard();
			cout<<") is LESS than the predicted card\nPlease try gain...";
		}
		else
		{
			if(compareCardFace==-1&&compareCardSuite==0)
			{
				cout<<"Face of the Card that you picked(";
				guess->getData()->showCard();
				cout<<") is LESS than the predicted card.However you managed to guess the correct suite\nPlease try gain...";
			}
			else
			{
				if(compareCardFace==-1&&compareCardSuite==1)
				{
					cout<<"Face of the Card that you picked(";
					guess->getData()->showCard();
					cout<<") is LESS than the predicted card and the suite that you picked up is HIGHER\nPlease try gain...";
				}
				else
				{
					if(compareCardFace==0&&compareCardSuite==-1)
					{
						cout<<"You have picked the right face(";
						guess->getData()->showCard();
						cout<<").However the suite that you picked up is LESS than the predicted card\nPlease try gain...";
					}
					else
					{
						if(compareCardFace==0&&compareCardSuite==1)
						{
							cout<<"You have picked the right face(";
							guess->getData()->showCard();
							cout<<").However the suite that you picked up is HIGHER\nPlease try gain...";
						}
						else
						{
							if(compareCardFace==1&&compareCardSuite==-1)
							{
								cout<<"The Face that you picked up is HIGHER(";
								guess->getData()->showCard();
								cout<<") and the suite that you picked up is LOWER than the prediced card\nPlease try gain...";
							}
							else
							{
								if(compareCardFace==1&&compareCardSuite==1)
								{
									cout<<"The Face that you picked up is HIGHER(";
									guess->getData()->showCard();
									cout<<") and the suite that you picked up is HIGHER than the prediced card\nPlease try gain...";
								}
								else
								{
									cout<<"The Face that you picked up is HIGHER(";
									guess->getData()->showCard();
									cout<<").However you managed to guess the correct suite\nPlease try gain...";
								}
							}
						}
					}
				}
			}
		}
	}
	
	vGuess[validGuessCount]=new Cards(guess->getData()->getFace(),guess->getData()->getSuite());
	validGuessCount++;
	getch();
	return(compare);
}

Node* CardGuessing::getCPCard()
{
	return(cpCard);
}

Node* CardGuessing::getMyGuessedCard()
{
	return(guess);
}

void CardGuessing::displayAllValidGuess(int pos)
{
	
	cout<<"\n-------------------------------------------------------------\n\n";
	cout<<"\n                     Game Result\n";
	cout<<"\n-------------------------------------------------------------\n\n";
	cout<<"  SNO\t\tCARD";
	cout<<"\n-------------------------------------------------------------\n\n";
	for(int i=0;i<pos;i++)
	{
		cout<<"\n"<<i+1<<" valid guess is :\t";
		vGuess[i]->showCard();
	}
	cout<<"\n-------------------------------------------------------------\n\n";
}

int CardGuessing::validateStringInput()
{
	int num;
	string sNumber;
	try
	{
		//cout<<"\nYour Input is :\t";
		//cin.ignore();
		getline(cin,sNumber);
		num=stoi(sNumber);
	}
	catch(invalid_argument &m)
	{
		cout<<"\nPlease enter a valid data <Only Number> [Spaces and special characters and strings not allowed]\n";
	}
	return(num);
}