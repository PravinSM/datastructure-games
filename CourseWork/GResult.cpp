#include"GResult.h"

GResult::GResult()
{
	
}

GResult::GResult(int round, string status, int bankPoints, int playerPoints,string description, int bankCardCount, int playerCardCount)
{
	this->round=round;
	this->status=status;
	this->bankPoints=bankPoints;
	this->playerPoints=playerPoints;
	this->description=description;
	this->bankCardCount=bankCardCount;
	this->playerCardCount=playerCardCount;
}

GResult::~GResult()
{

}

void GResult::setRounds(int round)
{	
	this->round=round;
}	

int GResult::getRounds()
{
	return(round);
}

void GResult::setStatus(string status)
{
	this->status=status;
}

string GResult::getStatus()
{
	return(status);
}

void GResult::setBankPoints(int bankPoints)
{
	this->bankPoints=bankPoints;
}

int GResult::getBankPoints()
{
	return(bankPoints);
}

void GResult::setPLayerPoints(int playerPoints)
{
	this->playerPoints=playerPoints;
}

int GResult::getPlayerPonits()
{
	return(playerPoints);
}

void GResult::setDescription(string description)
{
	this->description=description;
}

string GResult::getDescription()
{
	return(description);
}

void GResult::setBankCardCount(int bankCardCount)
{
	this->bankCardCount=bankCardCount;
}

int GResult::getBankCardCount()
{
	return(bankCardCount);
}

void GResult::setPlayerCardCount(int)
{
	this->playerCardCount=playerCardCount;
}

int GResult::getPlayerCardCount()
{
	return(playerCardCount);
}

void GResult::setData(int round, string status, int bankPoints, int bankCardCount,string description, int playerPoints, int playerCardCount)
{
	this->round=round;
	this->status=status;
	this->bankPoints=bankPoints;
	this->playerPoints=playerPoints;
	this->description=description;
	this->bankCardCount=bankCardCount;
	this->playerCardCount=playerCardCount;
}

ostringstream GResult::displayGResult()
{
	ostringstream result;
	result<<"\n    "<<round<<"\t"<<status<<"      "<<bankPoints<<"  \t   "<<bankCardCount<<"    \t   "<<playerPoints<<" \t\t   "<<playerCardCount;
	return(result);
}